﻿using System;
using System.IO;

namespace ExternalSort
{
    public class Utils
    {
        public static int MaxNumber = 1000000;

        public static void GenerateRandomFile(string fileName, int count)
        {
            Random random = new Random();
            using (StreamWriter writer = new StreamWriter(fileName, false, System.Text.Encoding.Default))
            {
                for (int i = 0; i < count; i++)
                {
                    writer.WriteLine(random.Next(MaxNumber));
                }
            }
        }

        public static void GenerateReverseFile(string fileName, int count)
        {
            using (StreamWriter writer = new StreamWriter(fileName, false, System.Text.Encoding.Default))
            {
                for (int i = count; i != 0; i--)
                {
                    writer.WriteLine(i);
                }
            }
        }

        public static string PrintFile(string fileName)
        {
            string result = "";
            using (StreamReader reader = new StreamReader(fileName, System.Text.Encoding.Default))
            {
                string line;
                while ((line = reader.ReadLine()) != null)
                {
                    result += Convert.ToInt32(line) + ", ";
                }
            }
            return result.Trim();
        }
    }
}