﻿using System;
using System.IO;

namespace ExternalSort
{
    public class MergeSort
    {
        public int FilesCount = 4;

        public bool AddFromFile(StreamReader f_in, StreamWriter f_out, ref int lastRead)
        {
            f_out.WriteLine(lastRead);
            int lastWritten = lastRead;
            int info = Convert.ToInt32(f_in.ReadLine());
            if (info == 0)
            {
                lastRead = 0;
                return false;
            }
            lastRead = info;
            return lastRead.CompareTo(lastWritten) >= 0;
        }

        public void Merge(StreamReader f1_in, StreamReader f2_in, StreamWriter f_out, ref int lastRead1, ref int lastRead2)
        {
            bool isSeriesF1 = lastRead1 != 0;
            bool isSeriesF2 = lastRead2 != 0;
            while (isSeriesF1 && isSeriesF2)
            {
                if (lastRead1.CompareTo(lastRead2) <= 0)
                    isSeriesF1 = AddFromFile(f1_in, f_out, ref lastRead1);
                else
                    isSeriesF2 = AddFromFile(f2_in, f_out, ref lastRead2);
            }
            if (isSeriesF1)
                while (AddFromFile(f1_in, f_out, ref lastRead1)) ;
            else if (isSeriesF2)
                while (AddFromFile(f2_in, f_out, ref lastRead2)) ;
        }

        public bool MergeSerieses(string f1_in, string f2_in, string f1_out, string f2_out)
        {
            StreamReader f1_read = new StreamReader(f1_in);
            StreamReader f2_read = new StreamReader(f2_in);
            StreamWriter[] f_write = new StreamWriter[] { new StreamWriter(f1_out), new StreamWriter(f2_out) };

            int lastRead1 = 0;
            int lastRead2 = 0;
            int info = Convert.ToInt32(f1_read.ReadLine());
            if (info != 0)
                lastRead1 = info;
            info = Convert.ToInt32(f2_read.ReadLine());
            if (info != 0)
                lastRead2 = info;
            int i = 0;
            while (lastRead1 != 0 || lastRead2 != 0)
            {
                Merge(f1_read, f2_read, f_write[i % 2], ref lastRead1, ref lastRead2);
                i++;
            }
            f1_read.Close();
            f2_read.Close();
            f_write[0].Close();
            f_write[1].Close();
            return i <= 1;
        }

        public void Sort(string inputfileName, string outputFileName)
        {
            string[] fileNames = new string[FilesCount];
            for (int j = 0; j < FilesCount; j++)
            {
                fileNames[j] = Guid.NewGuid().ToString() + ".txt";
                StreamWriter f = new StreamWriter(fileNames[j]);
                f.Close();
            }
            bool isSorted = MergeSerieses(inputfileName, fileNames[2], fileNames[0], fileNames[1]);
            int Index = 0;
            while (!isSorted)
            {
                if (Index == 0)
                    isSorted = MergeSerieses(fileNames[0], fileNames[1], fileNames[2], fileNames[3]);
                else
                    isSorted = MergeSerieses(fileNames[2], fileNames[3], fileNames[0], fileNames[1]);
                Index += 2;
                Index %= 4;
            }
            File.Copy(fileNames[Index], outputFileName, true);
            for (int j = 0; j < FilesCount; j++)
                File.Delete(fileNames[j]);
        }
    }
}
