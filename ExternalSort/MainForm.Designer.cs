﻿
namespace ExternalSort
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.textBoxForRandomNumbers = new System.Windows.Forms.TextBox();
            this.textBoxForReverseNumbers = new System.Windows.Forms.TextBox();
            this.labelForRandom = new System.Windows.Forms.Label();
            this.labelForReverse = new System.Windows.Forms.Label();
            this.buttonForGenerate = new System.Windows.Forms.Button();
            this.buttonForSort = new System.Windows.Forms.Button();
            this.textBoxForAmountNumbers = new System.Windows.Forms.TextBox();
            this.labelForAmountNumbers = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageResult = new System.Windows.Forms.TabPage();
            this.buttonForCriteriaSort = new System.Windows.Forms.Button();
            this.buttonForAddCount = new System.Windows.Forms.Button();
            this.numericUpDown = new System.Windows.Forms.NumericUpDown();
            this.labelForCountNumbers = new System.Windows.Forms.Label();
            this.dataGridView = new System.Windows.Forms.DataGridView();
            this.ColumnCountNumbers = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCriteria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ColumnCriteria2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.tabPageCheck = new System.Windows.Forms.TabPage();
            this.tabControl.SuspendLayout();
            this.tabPageResult.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).BeginInit();
            this.tabPageCheck.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // textBoxForRandomNumbers
            // 
            this.textBoxForRandomNumbers.Location = new System.Drawing.Point(56, 53);
            this.textBoxForRandomNumbers.Multiline = true;
            this.textBoxForRandomNumbers.Name = "textBoxForRandomNumbers";
            this.textBoxForRandomNumbers.Size = new System.Drawing.Size(400, 250);
            this.textBoxForRandomNumbers.TabIndex = 2;
            // 
            // textBoxForReverseNumbers
            // 
            this.textBoxForReverseNumbers.Location = new System.Drawing.Point(485, 53);
            this.textBoxForReverseNumbers.Multiline = true;
            this.textBoxForReverseNumbers.Name = "textBoxForReverseNumbers";
            this.textBoxForReverseNumbers.Size = new System.Drawing.Size(400, 250);
            this.textBoxForReverseNumbers.TabIndex = 3;
            // 
            // labelForRandom
            // 
            this.labelForRandom.AutoSize = true;
            this.labelForRandom.Location = new System.Drawing.Point(193, 34);
            this.labelForRandom.Name = "labelForRandom";
            this.labelForRandom.Size = new System.Drawing.Size(94, 13);
            this.labelForRandom.TabIndex = 4;
            this.labelForRandom.Text = "Случайные числа";
            // 
            // labelForReverse
            // 
            this.labelForReverse.AutoSize = true;
            this.labelForReverse.Location = new System.Drawing.Point(586, 34);
            this.labelForReverse.Name = "labelForReverse";
            this.labelForReverse.Size = new System.Drawing.Size(198, 13);
            this.labelForReverse.TabIndex = 5;
            this.labelForReverse.Text = "Числа заданные в обратном порядке";
            // 
            // buttonForGenerate
            // 
            this.buttonForGenerate.Location = new System.Drawing.Point(533, 322);
            this.buttonForGenerate.Name = "buttonForGenerate";
            this.buttonForGenerate.Size = new System.Drawing.Size(156, 85);
            this.buttonForGenerate.TabIndex = 6;
            this.buttonForGenerate.Text = "Сгенерировать";
            this.buttonForGenerate.UseVisualStyleBackColor = true;
            this.buttonForGenerate.Click += new System.EventHandler(this.buttonForGenerate_Click);
            // 
            // buttonForSort
            // 
            this.buttonForSort.Location = new System.Drawing.Point(715, 322);
            this.buttonForSort.Name = "buttonForSort";
            this.buttonForSort.Size = new System.Drawing.Size(156, 85);
            this.buttonForSort.TabIndex = 7;
            this.buttonForSort.Text = "Отсортировать";
            this.buttonForSort.UseVisualStyleBackColor = true;
            this.buttonForSort.Click += new System.EventHandler(this.buttonForSort_Click);
            // 
            // textBoxForAmountNumbers
            // 
            this.textBoxForAmountNumbers.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxForAmountNumbers.Location = new System.Drawing.Point(81, 365);
            this.textBoxForAmountNumbers.Multiline = true;
            this.textBoxForAmountNumbers.Name = "textBoxForAmountNumbers";
            this.textBoxForAmountNumbers.Size = new System.Drawing.Size(110, 30);
            this.textBoxForAmountNumbers.TabIndex = 8;
            this.textBoxForAmountNumbers.Text = "1000";
            this.textBoxForAmountNumbers.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.textBoxForAmountNumbers.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBoxForAmountNumbers_KeyPress);
            // 
            // labelForAmountNumbers
            // 
            this.labelForAmountNumbers.AutoSize = true;
            this.labelForAmountNumbers.Location = new System.Drawing.Point(88, 338);
            this.labelForAmountNumbers.Name = "labelForAmountNumbers";
            this.labelForAmountNumbers.Size = new System.Drawing.Size(98, 13);
            this.labelForAmountNumbers.TabIndex = 9;
            this.labelForAmountNumbers.Text = "Количество чисел";
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageResult);
            this.tabControl.Controls.Add(this.tabPageCheck);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(947, 504);
            this.tabControl.TabIndex = 10;
            // 
            // tabPageResult
            // 
            this.tabPageResult.Controls.Add(this.buttonForCriteriaSort);
            this.tabPageResult.Controls.Add(this.buttonForAddCount);
            this.tabPageResult.Controls.Add(this.numericUpDown);
            this.tabPageResult.Controls.Add(this.labelForCountNumbers);
            this.tabPageResult.Controls.Add(this.dataGridView);
            this.tabPageResult.Location = new System.Drawing.Point(4, 22);
            this.tabPageResult.Name = "tabPageResult";
            this.tabPageResult.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageResult.Size = new System.Drawing.Size(939, 478);
            this.tabPageResult.TabIndex = 0;
            this.tabPageResult.Text = "Исследование";
            this.tabPageResult.UseVisualStyleBackColor = true;
            // 
            // buttonForCriteriaSort
            // 
            this.buttonForCriteriaSort.Location = new System.Drawing.Point(541, 309);
            this.buttonForCriteriaSort.Name = "buttonForCriteriaSort";
            this.buttonForCriteriaSort.Size = new System.Drawing.Size(183, 109);
            this.buttonForCriteriaSort.TabIndex = 16;
            this.buttonForCriteriaSort.Text = "Исследовать";
            this.buttonForCriteriaSort.UseVisualStyleBackColor = true;
            this.buttonForCriteriaSort.Click += new System.EventHandler(this.buttonForCriteriaSort_Click);
            // 
            // buttonForAddCount
            // 
            this.buttonForAddCount.Location = new System.Drawing.Point(227, 395);
            this.buttonForAddCount.Name = "buttonForAddCount";
            this.buttonForAddCount.Size = new System.Drawing.Size(75, 23);
            this.buttonForAddCount.TabIndex = 15;
            this.buttonForAddCount.Text = "Добавить";
            this.buttonForAddCount.UseVisualStyleBackColor = true;
            this.buttonForAddCount.Click += new System.EventHandler(this.buttonForAddCount_Click);
            // 
            // numericUpDown
            // 
            this.numericUpDown.Location = new System.Drawing.Point(207, 346);
            this.numericUpDown.Maximum = new decimal(new int[] {
            999999999,
            0,
            0,
            0});
            this.numericUpDown.Name = "numericUpDown";
            this.numericUpDown.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown.TabIndex = 14;
            // 
            // labelForCountNumbers
            // 
            this.labelForCountNumbers.AutoSize = true;
            this.labelForCountNumbers.Location = new System.Drawing.Point(204, 309);
            this.labelForCountNumbers.Name = "labelForCountNumbers";
            this.labelForCountNumbers.Size = new System.Drawing.Size(98, 13);
            this.labelForCountNumbers.TabIndex = 12;
            this.labelForCountNumbers.Text = "Количество чисел";
            // 
            // dataGridView
            // 
            this.dataGridView.AllowUserToAddRows = false;
            this.dataGridView.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dataGridView.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView.ColumnHeadersHeight = 30;
            this.dataGridView.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnCountNumbers,
            this.ColumnCriteria,
            this.ColumnCriteria2});
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView.Location = new System.Drawing.Point(135, 21);
            this.dataGridView.Name = "dataGridView";
            this.dataGridView.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView.RowHeadersVisible = false;
            this.dataGridView.Size = new System.Drawing.Size(653, 214);
            this.dataGridView.TabIndex = 11;
            // 
            // ColumnCountNumbers
            // 
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColumnCountNumbers.DefaultCellStyle = dataGridViewCellStyle3;
            this.ColumnCountNumbers.HeaderText = "Количество элементов";
            this.ColumnCountNumbers.Name = "ColumnCountNumbers";
            this.ColumnCountNumbers.ReadOnly = true;
            this.ColumnCountNumbers.Width = 150;
            // 
            // ColumnCriteria
            // 
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.ColumnCriteria.DefaultCellStyle = dataGridViewCellStyle4;
            this.ColumnCriteria.HeaderText = "Значение критерия";
            this.ColumnCriteria.Name = "ColumnCriteria";
            this.ColumnCriteria.ReadOnly = true;
            this.ColumnCriteria.Width = 250;
            // 
            // ColumnCriteria2
            // 
            this.ColumnCriteria2.HeaderText = "Значение критерия";
            this.ColumnCriteria2.Name = "ColumnCriteria2";
            this.ColumnCriteria2.ReadOnly = true;
            this.ColumnCriteria2.Width = 250;
            // 
            // tabPageCheck
            // 
            this.tabPageCheck.Controls.Add(this.textBoxForRandomNumbers);
            this.tabPageCheck.Controls.Add(this.textBoxForReverseNumbers);
            this.tabPageCheck.Controls.Add(this.labelForAmountNumbers);
            this.tabPageCheck.Controls.Add(this.labelForRandom);
            this.tabPageCheck.Controls.Add(this.textBoxForAmountNumbers);
            this.tabPageCheck.Controls.Add(this.labelForReverse);
            this.tabPageCheck.Controls.Add(this.buttonForSort);
            this.tabPageCheck.Controls.Add(this.buttonForGenerate);
            this.tabPageCheck.Location = new System.Drawing.Point(4, 22);
            this.tabPageCheck.Name = "tabPageCheck";
            this.tabPageCheck.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageCheck.Size = new System.Drawing.Size(939, 478);
            this.tabPageCheck.TabIndex = 1;
            this.tabPageCheck.Text = "Проверка";
            this.tabPageCheck.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(967, 530);
            this.Controls.Add(this.tabControl);
            this.Name = "MainForm";
            this.Text = "External sort";
            this.tabControl.ResumeLayout(false);
            this.tabPageResult.ResumeLayout(false);
            this.tabPageResult.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView)).EndInit();
            this.tabPageCheck.ResumeLayout(false);
            this.tabPageCheck.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox textBoxForRandomNumbers;
        private System.Windows.Forms.TextBox textBoxForReverseNumbers;
        private System.Windows.Forms.Label labelForRandom;
        private System.Windows.Forms.Label labelForReverse;
        private System.Windows.Forms.Button buttonForGenerate;
        private System.Windows.Forms.Button buttonForSort;
        private System.Windows.Forms.TextBox textBoxForAmountNumbers;
        private System.Windows.Forms.Label labelForAmountNumbers;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageResult;
        private System.Windows.Forms.TabPage tabPageCheck;
        private System.Windows.Forms.DataGridView dataGridView;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCountNumbers;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCriteria;
        private System.Windows.Forms.DataGridViewTextBoxColumn ColumnCriteria2;
        private System.Windows.Forms.Label labelForCountNumbers;
        private System.Windows.Forms.Button buttonForAddCount;
        private System.Windows.Forms.NumericUpDown numericUpDown;
        private System.Windows.Forms.Button buttonForCriteriaSort;
    }
}

