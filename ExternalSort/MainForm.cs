﻿using System;
using System.Diagnostics;
using System.Windows.Forms;

namespace ExternalSort
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
            dataGridViewDraw();
        }
        private void dataGridViewDraw()
        {
            dataGridView.Rows.Add();
            dataGridView.Rows[0].Cells[1].Value = "Случайный файл";
            dataGridView.Rows[0].Cells[2].Value = "Обратный файл";
            dataGridView.ClearSelection();
        }

        private void textBoxForAmountNumbers_KeyPress(object sender, KeyPressEventArgs e)
        {
            char ch = e.KeyChar;
            if (!Char.IsDigit(ch) && ch != 8)
            {
                e.Handled = true;
            }
        }

        private void buttonForGenerate_Click(object sender, EventArgs e)
        {
            int amountNumbers = Convert.ToInt32(textBoxForAmountNumbers.Text);
            Utils.GenerateRandomFile("Random", amountNumbers);
            textBoxForRandomNumbers.Text = Utils.PrintFile("Random");

            Utils.GenerateReverseFile("Reverse", amountNumbers);
            textBoxForReverseNumbers.Text = Utils.PrintFile("Reverse");
        }

        private void buttonForSort_Click(object sender, EventArgs e)
        {
            MergeSort sort = new MergeSort();
            sort.Sort("Random", "Random");
            textBoxForRandomNumbers.Text = Utils.PrintFile("Random");
            sort.Sort("Reverse", "Reverse");
            textBoxForReverseNumbers.Text = Utils.PrintFile("Reverse");
        }

        private void buttonForAddCount_Click(object sender, EventArgs e)
        {
            int countNumbers = Convert.ToInt32(numericUpDown.Value);
            int countRow = dataGridView.Rows.Count;
            dataGridView.Rows.Add();
            dataGridView.Rows[countRow].Cells[0].Value = countNumbers;
        }

        private void buttonForCriteriaSort_Click(object sender, EventArgs e)
        {
            for (int i = 1; i < dataGridView.Rows.Count; i++)
            {
                MergeSort sort = new MergeSort();
                int countNumbers = Convert.ToInt32(dataGridView.Rows[i].Cells[0].Value);
                Utils.GenerateRandomFile("Random", countNumbers);
                Utils.GenerateReverseFile("Reverse", countNumbers);

                Stopwatch stopWatchRandom = new Stopwatch();
                stopWatchRandom.Start();
                sort.Sort("Random", "Random");
                stopWatchRandom.Stop();
                TimeSpan timeRandom = stopWatchRandom.Elapsed;

                Stopwatch stopWatchReverse = new Stopwatch();
                stopWatchReverse.Start();
                sort.Sort("Reverse", "Reverse");
                stopWatchReverse.Stop();
                TimeSpan timeReverse = stopWatchReverse.Elapsed;

                dataGridView.Rows[i].Cells[1].Value = timeRandom;
                dataGridView.Rows[i].Cells[2].Value = timeReverse;
            }
        }
    }
}